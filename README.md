# Ekylibre::PluginSystem

This Gem contains the core classes implementing the plugin system of the Ekylibre ERP.

It is built on top of a dependency injection container from the corindon Gem and is included in Rails through Rails::Railtie.

## Usage 

Useful links:
- [Ekylibre installation guide](https://github.com/ekylibre/ekylibre/wiki/Developer:-Install-Ekylibre-3.10-on-Ubuntu-18.04)
- [Plugin Developer's guide](doc/developer_guide.md)

## Other known open-source plugins

- Ekylibre
    - [MultiTenancy](https://gitlab.com/ekylibre/plugins/multi_tenancy): Allow Ekylibre to be deployed as a SaaS platform. This is one of the Core plugins.
    
