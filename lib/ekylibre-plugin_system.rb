# frozen_string_literal: true

require 'corindon'
require 'request_store'
require 'zeitwerk'

module Ekylibre # :nodoc:

end

loader = Zeitwerk::Loader.for_gem
loader.ignore(__FILE__)
loader.ignore("#{__dir__}/plugin_system/rails/**/*.rb")
loader.ignore("#{__dir__}/plugin_system/testing/**/*.rb")
loader.setup

require_relative 'ekylibre/plugin_system/rails/railtie' if defined?(::Rails)
