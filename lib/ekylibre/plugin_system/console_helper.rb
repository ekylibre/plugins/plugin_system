# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    # Helper methods to access the container from the Rails console
    module ConsoleHelper
      # Provides an easy access to the container from the console context.
      # @return [Corindon::DependencyInjection::Container]
      def container
        GlobalContainer.get
      end
    end
  end
end
