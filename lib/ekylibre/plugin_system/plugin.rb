# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    # Base class for a Plugin
    class Plugin
      # @return [Rails::Engine]
      attr_reader :engine

      # @param [Rails::Engine] engine
      def initialize(engine: nil)
        @engine = engine
      end

      # @return [String]
      def name
        self.class.name
      end

      def version
        'unknown'
      end

      # @param [Container] container
      def boot(container) end
    end
  end
end
