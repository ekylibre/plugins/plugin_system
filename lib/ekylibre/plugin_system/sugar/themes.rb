# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    module Sugar
      # Syntactic sugar to ease the registration of Themes and ThemeAddon into the container
      module Themes
        refine Plugin do
          using Corindon::DependencyInjection::Injectable

          # @param [Array<String>] addons
          # @param [Corindon::DependencyInjection::Container] container
          def register_theme_addons(addons, container:)
            addons.each do |relative_path|
              container.add_definition(
                self.class.make_definition(relative_path, Ekylibre::View::Themes::ThemeAddon) { tag 'ekylibre.view.theme_addon' },
                context: { path: relative_path, origin: self.name }
              ) do |ctx|
                args for_theme: value('tekyla'), relative_path: value(ctx.path), origin: value(ctx.name)
              end
            end
          end

          def register_themes(themes, container:)
            themes.each { |theme| register_theme(theme, container: container) }
          end

          def register_theme(theme, container:)
            container.add_definition(
              self.class.make_definition("themes.#{theme}", Ekylibre::View::Themes::Theme) { tag 'ekylibre.view.theme' },
              context: { theme: theme }
            ) do |ctx|
              args value(ctx.theme)
            end
          end
        end
      end
    end
  end
end
