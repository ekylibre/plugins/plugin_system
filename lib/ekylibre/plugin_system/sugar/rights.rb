# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    module Sugar
      # Syntactic sugar to ease the registration of Ekylibre Rights into the container
      module Rights
        refine Plugin do
          using Corindon::DependencyInjection::Injectable

          # @param [Corindon::DependencyInjection::Container] container
          # @param [String] origin
          def register_rights(container:, origin:)
            root_param = Corindon::DependencyInjection::Token::ValueToken.new(value: engine.root)
            origin_param = Corindon::DependencyInjection::Token::ValueToken.new(value: origin)

            container.add_definition(
              self.class.make_definition(
                'rights_loader',
                Ekylibre::Access::RightsLoader,
                root: root_param, origin: origin_param
              ) do
                tag 'ekylibre.access.rights_loader'
              end
            )
          end
        end
      end
    end
  end
end
