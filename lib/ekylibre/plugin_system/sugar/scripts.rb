# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    module Sugar
      # Syntactic sugar to ease the registration of ScriptAddon into the container
      module Scripts
        refine Plugin do
          using Corindon::DependencyInjection::Injectable

          # @param [Array<String>] addons
          # @param [Corindon::DependencyInjection::Container] container
          def register_script_addons(addons, container:)
            addons.each do |relative_path|
              container.add_definition(
                self.class.make_definition(relative_path, Ekylibre::View::Scripts::ScriptAddon) { tag 'ekylibre.view.script_addon' },
                context: { path: relative_path, origin: self.name }
              ) do |ctx|
                args relative_path: value(ctx.path), origin: value(ctx.name)
              end
            end
          end
        end
      end
    end
  end
end
