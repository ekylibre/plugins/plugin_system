# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    module Sugar
      # Syntactic sugar to ease the registration of Procedo procedures into the container
      module Procedo
        refine Plugin do
          using Corindon::DependencyInjection::Injectable

          # @param [Corindon::DependencyInjection::Container] container
          def register_procedures(container:)
            container.add_definition(
              self.class.make_definition('procedure_loader', ::Procedo::ProcedureLoader) { tag 'procedo.loader' },
              context: { root: engine.root }
            ) do |ctx|
              args root: value(ctx.root)
            end
          end
        end
      end
    end
  end
end
