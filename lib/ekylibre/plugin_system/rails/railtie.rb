# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    module Rails
      # Railtie to integrate the PluginSystem into a Rails application
      class Railtie < ::Rails::Railtie
        extend PluginRegistration

        initializer(:register_application) do |app|
          container = app.system.container

          container.set_parameter(Parameters::APPLICATION_ENV, ::Rails.env)
          container.set_parameter(Parameters::RAILS_APPLICATION, app)
        end

        initializer(:register_middleware, after: :engines_blank_point) do |app|
          app.config.middleware.insert_before(Warden::Manager, Middleware::RackMiddleware, app.system.container)
        end

        initializer(:register_sidekiq_middleware, after: :engines_blank_point) do |app|
          Middleware::SidekiqMiddleware.setup(container: app.system.container)
        end

        console do |app|
          # Make the app container accessible in the RequestStore when in rails console
          GlobalContainer.set(app.system.container)
          ActiveSupport::Reloader.to_prepare do
            GlobalContainer.set(app.system.container)
          end

          ::Rails::ConsoleMethods.send :include, ConsoleHelper
        end

        # Reboot system between requests if reloading is enabled.
        # Application configuration cannot be changed but this allow us to use rails reloading system in development.
        config.to_prepare do
          app = ::Rails.application

          app.system.reset
        end
      end
    end
  end
end
