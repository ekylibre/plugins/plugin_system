# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    # This class in the interface between the Rails application and the core plugin system that is basically just the container
    class System
      # @return [Corindon::DependencyInjection::Container]
      attr_reader :container

      def initialize
        @started = false
        @container = Corindon::DependencyInjection::Container.new
      end

      def start
        if !started?
          @started = true
          boot_plugins
        end
      end

      # @return [Boolean]
      def started?
        @started
      end

      def reset
        if !started?
          start
        else
          container.clear
        end
      end

      def plugins
        @container.tagged('ekylibre.system.plugin').map { |key| @container.get(key) }
      end

      private

        def boot_plugins
          plugins.each { |p| p.boot(container) }
        end
    end
  end
end
