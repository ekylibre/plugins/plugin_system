# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    VERSION = '0.7.0'
  end
end
