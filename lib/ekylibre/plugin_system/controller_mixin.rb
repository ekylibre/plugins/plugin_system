# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    # Helper module to access the container from a Rails controller
    module ControllerMixin
      # @return [Corindon::DependencyInjection::Container]
      def container
        request.env['container']
      end
    end
  end
end
