# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    # Utility module to facilitate Plugin and Engine registration into the plugin system
    module PluginRegistration
      class << self
        def extended(base)
          base.initializer(:register_railtie) do |app|
            container = app.system.container

            container.set_parameter(self.class, self)
          end
        end
      end

      def register_plugin(plugin_class)
        initializer(:register_plugin, after: :register_railtie) do |app|
          container = app.system.container

          container.add_definition(plugin_class)
        end
      end
    end
  end
end
