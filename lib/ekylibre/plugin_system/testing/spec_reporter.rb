# frozen_string_literal: true

require 'io/console'
require 'minitest/reporters'

module Ekylibre
  module PluginSystem
    module Testing
      class SpecReporter < Minitest::Reporters::SpecReporter
        def record_print_status(test)
          test_name = test.name.gsub(/^test_: /, 'test:')
          print_colored_status(test)
          print pad_test(test_name)
          print(format(' (%.2fs)', test.time)) unless test.time.nil?
          puts
        end

        private def pad_test(str)
          if ENV.key?('CI')
            super
          else
            pad(format("%-#{[TEST_SIZE, IO.console.winsize[1] - 15].max}s", str), TEST_PADDING)
          end
        end
      end
    end
  end
end
