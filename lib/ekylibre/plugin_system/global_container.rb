# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    # This class is a wrapper around RequestStore that, in addition to provide a concise way to access the container globally,
    # ensures that a container is present if the application ask for one (nil is not a valid value)
    class GlobalContainer
      class << self
        private :new

        # @param [Corindon::DependencyInjection::Container] container
        def set(container)
          RequestStore['container'] = container
        end

        def unset
          RequestStore.delete('container')
        end

        # @param [Corindon::DependencyInjection::Container] container
        def replace_with(container, &block)
          prev = if has?
                   get
                 else
                   nil
                 end

          set(container)

          block.call
        ensure
          if prev.nil?
            unset
          else
            set(prev)
          end
        end

        def has?
          RequestStore.exist?('container')
        end

        # @return [Corindon::DependencyInjection::Container]
        def get
          container = RequestStore['container']

          if container.nil?
            raise StandardError.new(
              'The application requires the container to be loaded to work. Please load it before calling GlobalContainer.get'
            )
          else
            container
          end
        end
      end
    end
  end
end
