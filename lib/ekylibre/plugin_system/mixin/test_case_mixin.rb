# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    module Mixin
      module TestCaseMixin
        def before_setup
          container = Ekylibre::Application.instance.system.container

          RequestStore.clear!

          Ekylibre::PluginSystem::GlobalContainer.set(container.dup)

          super
        end

        def after_teardown
          super
        ensure
          Ekylibre::PluginSystem::GlobalContainer.unset
        end

        # @return [Corindon::DependencyInjection::Container]
        def container
          Ekylibre::PluginSystem::GlobalContainer.get
        end
      end
    end
  end
end
