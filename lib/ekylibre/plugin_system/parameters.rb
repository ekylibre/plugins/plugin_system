# frozen_string_literal: true

using Corindon::DependencyInjection::Injectable

module Ekylibre
  module PluginSystem
    # Utility class defining some InjectionTokens for the plugin system
    class Parameters
      APPLICATION_ENV = make_parameter('application_env')
      RAILS_APPLICATION = make_parameter('rails_application')
    end
  end
end
