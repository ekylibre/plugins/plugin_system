# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    module Middleware
      # Sidekiq server middleware adding the provided container to the job context.
      # It also makes the container accessible through RequestStore for the legacy code to be able to access it globally.
      module SidekiqMiddleware
        class ServerMiddleware
          def initialize(container)
            @container = container
          end

          def call(_worker_class, item, _queue)
            GlobalContainer.replace_with(container) do
              item['container'] = container

              yield
            end
          ensure
            # We want to have the same behavior as the request_store-sidekiq gem.
            # However, we want to insert our middleware BEFORE the Batch middleware.
            RequestStore.clear!
          end

          private

            # @return [Corindon::DependencyInjection::Container]
            attr_reader :container
        end
      end
    end
  end
end
