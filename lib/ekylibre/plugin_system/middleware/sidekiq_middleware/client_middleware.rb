# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    module Middleware
      # Sidekiq client middleware adding the provided container to the job context
      # so that all other middlewares can access it for configuring newly enqueued jobs.
      module SidekiqMiddleware
        class ClientMiddleware
          def call(worker_class, item, queue, redis_pool = nil)
            item['container'] = GlobalContainer.get

            yield
          end
        end
      end
    end
  end
end
