# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    module Middleware
      module SidekiqMiddleware
        class << self
          # This method sets up sidekiq middlewares allowing to access the container natively inside jobs.
          #
          # In addition, it also stores the job's container inside RequestStore so that
          # the legacy code of Ekylibre can globally access the container when needed.
          #
          # The client middleware is called when a job is enqueued, the server middleware when the job is executed.
          #
          def setup(container:)
            ::Sidekiq.configure_client do |config|
              configure_client(config)
            end

            ::Sidekiq.configure_server do |config|
              configure_client(config)

              configure_server(config, container: container)
            end
          end

          private

            def configure_client(config)
              config.client_middleware do |chain|
                chain.add ClientMiddleware
              end
            end

            def configure_server(config, container:)
              config.server_middleware do |chain|
                if defined?(::Sidekiq::Batch::Server)
                  chain.insert_before ::Sidekiq::Batch::Server, ServerMiddleware, container
                else
                  chain.add ServerMiddleware, container
                end
              end
            end
        end
      end
    end
  end
end
