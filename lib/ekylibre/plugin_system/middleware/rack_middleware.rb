# frozen_string_literal: true

module Ekylibre
  module PluginSystem
    module Middleware
      # Rack middleware adding the provided container to the request context
      class RackMiddleware
        # @param [#call] app
        # @param [Corindon::DependencyInjection::Container] container
        def initialize(app, container)
          @app = app
          @container = container
        end

        def call(env)
          # request_container = enter_tenant(container, ::Ekylibre::Tenant.current)
          GlobalContainer.replace_with(container) do

            # We HAVE to mutate the environment because Devise (and maybe other) needs to pass information up the middleware stack
            # from the Rails application to the Auth middleware through the environment
            env['container'] = container

            @app.call(env)
          end
        end

        private

          # @return [Corindon::DependencyInjection::Container]
          attr_reader :container
      end
    end
  end
end
