Ekylibre Plugin System, requirements
-----

# State of the art of plugins in Ekylibre as of 26/10/2020
See [V1 description](../V1/index.md)

# Requirements
The Core is maintained by the 'Edition' team and a __strong__ accent is put into having a good code quality codebase with a growing test base and code coverage. The Core contains all the base features of Ekylibre.  

[comment]: <> (PROCESS: Before becoming the implementation of a new feature, the decision on whether it belongs to the Core should be made.)

The injection of additional Controller/Views/Routes/etc. MUST be done through the Rails method of extension: `Rails::Engine`

Plugins MUST NOT alter in any form any of the classes from the Core of Ruby in general. __NO monkey-patching is allowed__

## Ekylibre Services
All of these requirement MUST be fulfilled WITHOUT any specific change in the Core of Ekylibre.

A plugin MUST be able to extend the core services of Ekylibre:
- __Procedo__ by providing additional procedures
- __Navigation__ to be able to add Menu elements in the interface
- __Rights__ to restrict access to certain parts on the application based on the roles of the user
- __Exchangers__ to provide additional means of importing data from a wide variety of files
- __Themes__ for the user to choose from or increase brand recognition
- __Ekylibre::View::Addons__ to be able to inject view partials in specific points in the views defined by the Core. (For this particular changes, required injection points can be added through merge requests to the Core)
- __Cells__ that can be selected from a list and added into Dashboards

## Additional models
A Plugin SHOULD avoid the addition of specific models. Addition of models through a MR to the Core SHOULD be preferred.

If the plugins needs specific models that make no sens to be added to the Core, the following rules MUST be followed:
- class names MUST be namespaced to prevent name collision with Ekylibre models and other Plugins
- database table names MUST be prefixed by the plugin name for the same reasons
- Relations between models MUST be unidirectional as no modification of the Core are allowed
- Database migrations MUST be fully-reversible and simple SQL DELETE statements MUST be enough to clear the database if the plugin is uninstalled.

## API and GraphQL
Still needs to be installed in the Core. Technical limitations are not known yet. However:
- A Plugin SHOULD be able to provide additional Resources to the GraphQL endpoint.

## Deployment
A Flavor of Ekylibre is defined as the Core of Ekylibre to which is added a specific list of Plugins.

The deployment of a Flavor MUST be easily reproducible:
- A specific version of the Core and all of its added plugins MUST be used
- Third party Gems and javascript packages MUST have a specific version used (lockfiles)
- The deployment MUST NOT contain any additional application code, everything MUST be in the Core or Plugins

