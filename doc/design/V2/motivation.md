Why Ekylibre Plugin System is done like this?
-----

# What is a Plugin?
The word Plugin (with a capital P) is the extension concept called internally at Ekylibre that allow the developper to add functionnality to the core of Ekylibre by means of a specific API.
It should be distributed in a Gem and be decoupled from the core in the sense of _the core should not contain any specific code to have this plugin enabled_. (Except for Models, more on that later).

For more information, see:
- [Overview](../../overview.md)
- [Requirements](requirements.md)
- [Implementation](../../developer_guide.md)

# A brief state of the art analysis on how to extend Rails applications
As the main reason to write this document is to show the limitations of the customizations options available natively in Rails to write plugins, the edge guide (Rails 6.0) is used.

This is written based on documentation available on the [Rails Edge Guides](https://edgeguides.rubyonrails.org/) even if at the date of writing, the current version used in Ekylibre is 4.2.11.  
Because most of the experiments were done on Rails 4.2, additions in Rails 5 or 6 are not (fully) covered.

## Rails initialization process
The `bin/rails` script (either for the _console_ or _server_ subcommand) does, in order:

    1. require `config/application.rb`  
        1.1. require `config/boot.rb`  
            1.1.1. use the Gemfile provided in the environment or use `Gemfile` as default  
            1.1.2. Sets up bundler by requiring `bundler/setup`
       1.2. require the whole Rails libraries through `require 'rails/all'`  
       1.3. require all the gems listed in the default group + the group of the `RAILS_ENV` environment variable    
       1.4. defines the project `Application` class that inherits from `Rails::Application`  
            1.4.1. The `Rails::Application` defines the `inherited` ruby callback that is called when evaluating the `  class Application < Rails::Application` line    
                1.4.1.1. This internally sets the Rails application class to `YourModule::Application` (and explains why you can't have multiple class extending `Rails::Application`)  
                1.4.1.2. It runs the `before_configuration` hooks that have been registered by engines or railties  
            1.4.2. Then and _only then_ it executes the body of the `Application` class that is being defined.  
    2. require the `rails/command` file that defines the commands and invoke the one that is called  
       2.1. This command calls `Rails.application` that instanciate (if it was not done previously) the class registered at __1.4.1.1__  
       2.2. Run the rest of the command.  
   
### What about Application, Engine, Railtie?
The class hierarchy is Application < Engine < Railtie.  
Railtie defines a callback that registers in the parent class the class of all its direct descendents. The registered Engines and Railties are then gathered from the Engine and Railties classes.   
This creates a limitation on how Railties can be defined: they can't be subclassed or else the new Engine won't be picked up by Rails.

[comment]: <> (This is not completely true:)
[comment]: <> (TODO: investigate this)
[comment]: <> (The classes being registered at load time, and the gems being loaded before loading the application classes &#40;through `Bundler.require&#40;*Rails.groups&#41;`&#41;, Railties should not, then be able to redefine/extend classes from the application as they are loaded first.<)

When the application boots, initializers are collected and run.

### Collecting initializers
Initializers for an Application are the initializers of all its ancestors.  
As the Application class has an ancestor chain of Rails::Application, Rails::Engine and Rails::Railtie, all the initializers defined by them are gathered.

#### Initializers for Rails::Railtie
Railtie does not define any specific initializers but contains all the plumbing to gather them if they are defines is direct subclasses.  
_Interesting_ specificity, here: Railtis has a list of `ABSTRACT_RAILTIES` preventing anyone, without patching the Railtie class, to add a custom Railtie class tree.

Initializers are registered through the `initializer` DSL. By default, initializers are ran, in a given Railtie, in the order they are defined. This behavior can be overriden by providing an explicit initializer as dependency.

#### Initializers for Rails::Engine
An Engine is a Railtie that contains a Rails application directory structure and defines controllers, views, etc.
Default initializers are:
- set_load_path: adds the default folders to the load path
- set_autoload_paths: configures autoload
- set_eager_load_path: configures eager loading
- add_routing_paths: configures routing using `config/routes.rb`
- add_locales: adds `config/locales` to the i18n load paths
- add_views_paths: prepend the `app/views` directory to view path if it exist
- load_environment_config: requires the files in `config/environments`
- prepend_helpers_path: prepend the helpers in `app/helpers`
- load_config_initializer: insert an initializer for each file in `config/initializers` to be ran after itself.
- engines_blank_point: this initializer is just a blank placeholder so that all custom initializers defined in the subclasses of Engine are ran after it. (see the part on initializer sorting for more explanation on this)

### Initializers for Rails::Application
An Application is a Rails::Engine and inherits all of its initializers.

It prepends and appends initializers defined in the `Bootstrap` and `Finisher` classes to its own initializers.

### Initializer sorting and execution
The iniitalizers are sorted using a topological sort so that all initializers are ran after their dependencies.
In addition, for engines, all initializers with the same name are ran together. (Example: all the engines are executing the `load_config_initializer` before calling `engines_blank_point`.  

### More initialization callbacks
Rails provides, in development mode, a middleware that is responsible to reload the application. Callbacks can be added at this point to be executed for each request when a file change has been detected.
Based on the Rails version, there are different callbacks available:
- Rails 4 only defines one point that is called `to_prepare` that is called before each request
- Rails 5 and 6 have more configuration options to have code called before unloading and after reloading.

Note here that `to_prepare` callbacks are also ran in production. But only once after loading the application.

## Plugins
The first mean of extension listed in the edge guides (Rails 6) is [plugins](https://edgeguides.rubyonrails.org/plugins.html).

The guide goes through the steps of creating a plugin that is able to patch the environment by reopening Ruby classes.  
The [part 4](https://edgeguides.rubyonrails.org/plugins.html#add-an-acts-as-method-to-active-record) describes the way to define `acts_as` extensions to models. These kind of extension need a method called `acts_as_*` to be called on the model that uses it.
In order to be able to extend core classes, the core needs to explicitely call this method in its models.

### Why not?
This method of extension does not meets the need of Plugins (with the Ekylibre meaning) we have. Rails plugins _need_ to be known from the core application tu be used without monkey-patching classes/models to dynamically add new behaviors.

Language extensions like the Ruby/Rails ecosystem seems to be eager to write and use should also be considered harmful as they encourage the use of an all-global state and God-classes that are known to be anti-patterns across most (if not all) other programming languages.

## Engines
According to the [Rails documentation](https://edgeguides.rubyonrails.org/engines.html):
> Engines are miniature applications that provide functionality to their host applications.

They provide routes/controllers/models/views/etc. and configuration for them.

Engines are the way to add functionality to a Rails application. They allow to add elements in all the parts of the framework.  
They also provide a way to:
> [...] add and/or override engine MVC functionality in the main Rails application.

### Why (its) not (enough) ?
Its clear here that Engines needs to be used in the Plugin system as they allow to define independent sub-apps. However, not everything in the Rails Engines should be used.

Despite being at the center of the Rails extension system, they are not able to provide out of the box, a way to extend custom components without patching them. It will not be possible to cleanly extend Ekylibre components through Engines in a flexible enough way.

## Load and Configurations Hooks
While in the Engine part of the documentation, this means of extension can be used without an Engine. It describes a way to hook code to be executed when a specific class for the main components of the Rails framework are loaded to avoid the Engine to load the entire framework unnecessarily.

### Why not?
This is only a mean to globally patch at runtime a framework class. Since the patching is registered and executed at load time (when classes are loaded in memory), all the behavior patching can only be based on globally accessible variable and constants.

Even though it seems not to be a concern to most of the Ruby ecosystem, patching classes you don't own at runtime should be considered harmful and is error-prone. Its also tied to the implementation details of the patched class and not its public API.

# Where does this leaves us?
Basically, the main flaws of the Rails framework and Ruby ecosystem are:
- Global state everywhere
- Everything is accessible everywhere
- Encouraging the use of monkeypatch to implement custom functionalities instead of relying on specific extension points in the components.
- God classes everywhere (Controllers+View Helpers, Activerecord models that contains logic+persistence+validation+data)

Even though Rails allow the developer to override Controllers and Views, its __only__ possible when the Core wants to override behavior of the Engines and not the other way around and its precisely what we want to achieve.

To add more complexity, the plugin system should be able to play well with Rails reloading in development mode.

