Ekylibre Plugin System, Version 1

# Overview
Two systems to separate code in modules are available in Ekylibre:
- Plugins (V1)
- Gems

## Plugins
This is an internal legacy system used by many plugins:
- Hajimari
- Cap
- Ednotif
- Cap2020

Pros:
- Manifest allowing to override routes and inject partials (only cobbles and toolbar) in a centralized fashion (`Plugfile`)
- Registry of loaded plugins accessible from the core
- Custom loading of the whole plugin tree (controller/views/etc.)

Cons:
- Custom system: need to update the system for each ne need
- The core is not able to declare a dependency on a particular Plugin except from the Git submodule
- Folder organization is fixed and cannot be easily changed
- Tests can only be run from the core

Examples:
- Dependency Cap -> Hajimari ????
- Route overloading: tt plugin ????
- Cobble and toolbar injection: ednotif
- list of loaded plugins: Saassy

## Gems
More recent system based on the Gem mechanism of Ruby and Rails Engine

Pros:
- Gem registry in the `Gemfile`
- Dependencies are managed by Bundler
- List of loaded Gems is available in the application. Rails also provides a list of loaded Engines.
- Rails Engines are managing te paths like for the main application.
  The use of Rails Engine is also simplifying all the logic of adding routes/controllers/etc.
- No need of an additional repository with custom code (and merge conflicts on update)
- Tests can be done with only the Gem
- ~~The code from the core can be overloaded by the Gem~~ NO! View partial overloading (and same for controllers) is working the other way around (at least officially in the Rails Guide)

Cons:
- No DSL for configuration like in the Plugins
- No restriction on what part of the core can (and should) be overloaded from the Gem.

Examples:
- Zoning
- Planning