Ekylibre Plugin System, developer's guide
----

# Getting started
## Create a Plugin class
Plugins are subclasses of `Ekylibre::PluginSystem::Plugin`. They can specify a custom name if the default is not satisfactory by overriding
the `Plugin#name` method.  
They should be `Injectable` and be tagged with the `ekylibre.system.plugin` tag.

```ruby
using Corindon::DependencyInjection::Injectable

module MyModule
  class Plugin < Ekylibre::PluginSystem::Plugin
    injectable do
      args engine: param(MyModule::Rails::Engine)
      tag 'ekylibre.system.plugin'
    end
  end
end
```

## Registering the plugin
A plugin should be registered in the Application through `Rails::Engine` by means of an `initializer`

```ruby
module MyModule
  module Rails
    class MyEngine < ::Rails::Engine
      extend Ekylibre::PluginSystem::PluginRegistration

      register_plugin(MyModule::Plugin)
    end
  end
end
```

Nothing prevents an Engine to register multiple plugins.

## Defining services
### Extend Ekylibre core
The Ekylibre core can be extended in multiple places through the registrations of services into the container.  
For the more common use-cases, some syntactic sugar is provided in the form of refinments in the `Ekylibre::PluginSystem::Sugar` module.

#### Access rights
An access rights file can be provided to extend the Right system of Ekylibre

```ruby
using Corindon::DependencyInjection::Injectable
using Ekylibre::PluginSystem::Sugar::Rights

module MyModule
  class Plugin < Ekylibre::PluginSystem::Plugin
    injectable do
      args engine: param(MyModule::Rails::Engine)
      tag 'ekylibre.system.plugin'
    end

    # @param [Corindon::DependencyInjection::Container] container
    def boot(container)
      # In order for this call to work, the config/rights.yml file should be present.
      register_rights(container: container)
    end
  end
end
```

#### Registering new procedures to Procedo
```ruby
using Corindon::DependencyInjection::Injectable
using Ekylibre::PluginSystem::Sugar::Procedo

module MyModule
  class Plugin < Ekylibre::PluginSystem::Plugin
    injectable do
      args engine: param(MyModule::Rails::Engine)
      tag 'ekylibre.system.plugin'
    end

    # @param [Corindon::DependencyInjection::Container] container
    def boot(container)
      # Procedures should be stored in app/config/procedures
      register_procedures(container: container)
    end
  end
end
```

#### Script addons
Theme addons are SCSS files that are included in the application js through Sprockets `//=require`.

```ruby
using Corindon::DependencyInjection::Injectable
using Ekylibre::PluginSystem::Sugar::Scripts

module MyModule
  class Plugin < Ekylibre::PluginSystem::Plugin
    injectable do
      args engine: param(MyModule::Rails::Engine)
      tag 'ekylibre.system.plugin'
    end

    # @param [Corindon::DependencyInjection::Container] container
    def boot(container)
      # paths should be relative to app/assets/javascripts
      register_script_addons(%i[path/to/file1 path/to/file2], container: container)
    end
  end
end
```

#### Theme addons
Theme addons are SCSS files that are included in the main stylesheet through a `@include` call.

```ruby
using Corindon::DependencyInjection::Injectable
using Ekylibre::PluginSystem::Sugar::Themes

module MyModule
  class Plugin < Ekylibre::PluginSystem::Plugin
    injectable do
      args engine: param(MyModule::Rails::Engine)
      tag 'ekylibre.system.plugin'
    end

    # @param [Corindon::DependencyInjection::Container] container
    def boot(container)
      # paths should be relative to app/assets/stylesheets
      register_theme_addons(%i[path/to/file1 path/to/file2], container: container)
    end
  end
end
```

### The native way
Services should be added to the Container with the `Plugin#boot` method. This method is called only once when the Application is initialized.
See the [Corindon::DependencyInjection component documentation](https://gitlab.com/piotaixr/corindon/-/blob/master/README.md) for more insight on what is possible to do with the Container instance.

```ruby
# lib/my_module/my_service.rb
using Corindon::DependencyInjection::Injectable

module MyModule
    class MyService
      # Information for the container that MyService depends on Ekylibre::AnOtherService
      injectable(Ekylibre::AnOtherService)
      
      # When building MyService, the container will first build an instance of Ekylibre::AnOtherService and 
      # provide it as parameter in the initializer
      def initialize(other_service); end
    end
end

# lib/my_module/plugin.rb
module MyModule
  class Plugin < Ekylibre::PluginSystem::Plugin

    def boot(container)
      super(container)

      container.register(MyModule::MyService)
    end
  end
end
```

## Accessing services
### Preferred way: in the container
The container where all services are registered is accessible in the controllers through the `container` method provided by the `Ekylibre::PluginSystem::ControllerMixin` module that should be included in the `ApplicationController`.

```ruby
class MyController
  def index
    # If RandomCoreService can be built by the container, service will contain the service instance 
    service = container.get(Ekylibre::RandomCoreService)
    
    render service.path
  end
end
```

### Outside of the container
A middleware is inserted that allow the container to be globally available in the scope of the thread handling the current request. This is done with the `request_store` gem.  
The container can be retrieved through `RequestStore['container']` and used like in the controllers.  
__When possible, prefer to access the container only from the controller__

### In sidekiq jobs
The container can be accessed in sidekiq jobs via the use of the Gem `request_store-sidekiq` that provides the `RequestStore` API in jobs.  

For jobs, prefer accessing the `RequestStore` only at the root of your job to create the needed services and avoid accessing it in the logic part of the job code to maintain a good separation of domains.
