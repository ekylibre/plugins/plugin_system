# frozen_string_literal: true

require_relative 'lib/ekylibre/plugin_system/version'

Gem::Specification.new do |spec|
  spec.name = 'ekylibre-plugin_system'
  spec.version = Ekylibre::PluginSystem::VERSION
  spec.authors = ['Ekylibre developers']
  spec.email = ['dev@ekylibre.com']

  spec.summary = 'Plugin system for Rails built around a Dependency Injection Container'
  spec.required_ruby_version = '>= 2.6.0'
  spec.homepage = 'https://www.ekylibre.com'
  spec.license = 'AGPL-3.0-only'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.glob(%w[lib/**/*.rb *.gemspec])

  spec.require_paths = ['lib']

  spec.add_dependency 'corindon', '~> 0.8.0'
  spec.add_dependency 'railties', '> 4'
  spec.add_dependency 'request_store', '~> 1.5.0'
  spec.add_dependency 'zeitwerk', '~> 2.4'

  spec.add_development_dependency 'bundler', '>= 1.17'
  spec.add_development_dependency 'minitest', '~> 5.14'
  spec.add_development_dependency 'minitest-reporters', '~> 1.4'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rubocop', '~> 1.11.0'
end
