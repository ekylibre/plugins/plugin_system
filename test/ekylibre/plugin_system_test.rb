require 'test_helper'

module Ekylibre
  class PluginSystemTest < Minitest::Test
    def test_that_it_has_a_version_number
      refute_nil ::Ekylibre::PluginSystem::VERSION
    end
  end
end
