# frozen_string_literal: true

require 'test_helper'

using Corindon::DependencyInjection::Injectable

describe Ekylibre::PluginSystem::System do
  class DummyPlugin < Ekylibre::PluginSystem::Plugin
    injectable do
      args engine: value(:engine)
      tag 'ekylibre.system.plugin'
    end

    def initialize(engine:)
      super(engine: engine)
    end
  end

  before do
    @system = Ekylibre::PluginSystem::System.new
  end

  it 'should allow definition of plugins by class' do
    @system.container.add_definition(DummyPlugin)

    plugins = @system.plugins
    assert_equal 1, plugins.size
    assert_instance_of DummyPlugin, plugins.first
  end

  it 'should reinstanciate class plugins after a reboot' do
    @system.container.add_definition(DummyPlugin)
    @system.start

    plugins = @system.plugins
    assert_equal 1, plugins.size

    @system.reset
    other_plugins = @system.plugins
    assert_equal 1, plugins.size
    refute_same plugins.first, other_plugins.first
  end
end
