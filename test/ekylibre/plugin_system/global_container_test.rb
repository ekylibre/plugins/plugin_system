# frozen_string_literal: true

require 'test_helper'

describe Ekylibre::PluginSystem::GlobalContainer do
  before do
    RequestStore.clear!
  end

  describe :has? do
    it 'returns false if no container present in RequestStore' do
      refute Ekylibre::PluginSystem::GlobalContainer.has?
    end

    it 'returns true if RequestStore has a value for container' do
      RequestStore['container'] = 42

      assert Ekylibre::PluginSystem::GlobalContainer.has?
    end
  end

  describe 'set' do
    it 'sets the value if none present' do
      Ekylibre::PluginSystem::GlobalContainer.set(42)

      assert_equal 42, RequestStore['container']
    end

    it 'replaces the value if already present' do
      RequestStore['container'] = 42

      Ekylibre::PluginSystem::GlobalContainer.set(43)

      assert_equal 43, RequestStore['container']
    end
  end

  describe :get do
    it 'raise an exception if no container is present' do
      assert_raises StandardError do
        Ekylibre::PluginSystem::GlobalContainer.get
      end
    end

    it 'returns the value from the RequestStore if present' do
      RequestStore['container'] = 42
      assert_equal 42, Ekylibre::PluginSystem::GlobalContainer.get
    end
  end

  describe :unset do
    it 'removes the "container" key from the store' do
      RequestStore['container'] = 42
      Ekylibre::PluginSystem::GlobalContainer.unset
      refute RequestStore.exist?('container')
    end

    it 'is works even if no container is set' do
      refute RequestStore.exist?('container')
      Ekylibre::PluginSystem::GlobalContainer.unset
      refute RequestStore.exist?('container')
    end
  end

  describe :replace_with do
    it 'calls the provided block' do
      called = false

      Ekylibre::PluginSystem::GlobalContainer.replace_with(43) do
        called = true
      end

      assert called
    end
    it 'replaces the container only inside the provided block' do
      Ekylibre::PluginSystem::GlobalContainer.set(42)

      Ekylibre::PluginSystem::GlobalContainer.replace_with(43) do
        assert_equal 43, Ekylibre::PluginSystem::GlobalContainer.get
      end

      assert_equal 42, Ekylibre::PluginSystem::GlobalContainer.get
    end

    it 'unsets the container if none present' do
      Ekylibre::PluginSystem::GlobalContainer.replace_with(42) do
        # noop
      end

      refute Ekylibre::PluginSystem::GlobalContainer.has?
    end
  end
end
