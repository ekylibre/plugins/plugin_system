# frozen_string_literal: true

require 'test_helper'

describe Ekylibre::PluginSystem::Middleware::RackMiddleware do
  before do
    RequestStore.clear!
  end

  it 'should set the container env (and RequestStore) variable' do
    container = Corindon::DependencyInjection::Container.new
    environment = {}

    called = false
    app = proc do |env|
      called = true
      assert_equal environment, env
      assert RequestStore.exist?('container')
      assert_equal container, RequestStore.fetch('container')
    end

    middleware = Ekylibre::PluginSystem::Middleware::RackMiddleware.new(app, container)

    refute RequestStore.exist?('container')
    middleware.call(environment)

    assert called
    assert environment.key?('container')
    assert_equal container, environment.fetch('container')
  end
end
