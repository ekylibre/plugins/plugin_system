# frozen_string_literal: true

require 'test_helper'

describe Ekylibre::PluginSystem::Middleware::SidekiqMiddleware::ServerMiddleware do
  before do
    RequestStore.clear!
  end

  it 'should set the container env variable' do
    container = Corindon::DependencyInjection::Container.new
    environment = {}

    middleware = Ekylibre::PluginSystem::Middleware::SidekiqMiddleware::ServerMiddleware.new(container)

    refute RequestStore.exist?('container')

    called = false
    middleware.call(Class, environment, :queue_name) do
      called = true
      assert RequestStore.exist?('container')
      assert_equal container, RequestStore.fetch('container')
    end

    assert called
    assert environment.key?('container')
    assert_equal container, environment.fetch('container')
  end
end
