# frozen_string_literal: true

require 'test_helper'

describe Ekylibre::PluginSystem::Middleware::SidekiqMiddleware::ClientMiddleware do
  it 'should set the container env variable' do
    container = Corindon::DependencyInjection::Container.new
    environment = {}

    middleware = Ekylibre::PluginSystem::Middleware::SidekiqMiddleware::ClientMiddleware.new

    called = false
    Ekylibre::PluginSystem::GlobalContainer.replace_with(container) do
      middleware.call(Class, environment, :queue_name) do
        called = true
      end
    end

    assert called
    assert environment.key?('container')
    assert_equal container, environment.fetch('container')
  end
end
