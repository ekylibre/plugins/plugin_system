# frozen_string_literal: true

require_relative '../lib/ekylibre-plugin_system'
require_relative '../lib/ekylibre/plugin_system/testing/spec_reporter'

require 'minitest/autorun'

Minitest::Reporters.use! Ekylibre::PluginSystem::Testing::SpecReporter.new
